import express, {Response, Request, NextFunction} from 'express';
import { getNewsRoute } from '../controllers/getNews';
import { checkUserRoute } from '../middlewares/checkUserRole';

const router = express.Router();

router.get('/', (req: Request, res: Response) =>{ res.send(`It's alive!`) });
router.get('/getNews', checkUserRoute, getNewsRoute );

export default router;