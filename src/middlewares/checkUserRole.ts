import { Request, Response, NextFunction } from 'express';

const checkUserRoute = async (req: Request, res: Response, next: NextFunction) => {
    if ('Logged'){
        next();
    }else{
        res.status(401).send('Unauthorized');
    }
};

export { checkUserRoute };
