import { Request, Response, NextFunction } from 'express';
import { getNews } from '../services/getNews';

const getNewsRoute = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const response = await getNews(req, res, next);
        return res.json(response);
    } catch (err) {
        next(err);
    }
};

export { getNewsRoute };
