import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const swaggerDefinition = {
    
    info: {
        title: 'Swagger boilerplate',
        version: '0.1.0',
        description: 'This API does SOMETHING',
    },
    openapi: '1.0.0',
    servers: [
        {
            url: '/api/v1'
        }
    ]
};

const options = {
    swaggerDefinition,
    apis: ['swagger/docs/**/specification.yaml'],
};
const swaggerSpec = swaggerJSDoc(options);

export default (path: any, app: any) => app.use(path, swaggerUi.serve, swaggerUi.setup(swaggerSpec));
