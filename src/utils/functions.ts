import { IncomingHttpHeaders } from 'http';

const setHeaders = (token: string, incomingHeader: IncomingHttpHeaders): any => {
    const headers = {
        Accept: 'application/json'
    };

    return headers;
};

const buildQueryParams = (uriPath: string, querySearch: any, queryParams: Array<string>) => {
    var uriQueryPath: string;
    uriQueryPath = '?';
    queryParams.forEach((key) => {
        var value = querySearch[key];
        if (value !== undefined) uriQueryPath = uriQueryPath.concat(key).concat('=').concat(value).concat('&');
    });
    uriQueryPath = uriQueryPath.substr(0, uriQueryPath.length - 1);
    uriPath = uriPath.concat(uriQueryPath);
    return uriPath;
};

export { setHeaders, buildQueryParams };
