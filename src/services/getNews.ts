import { NextFunction, Request, Response } from 'express';
import { setHeaders } from '../utils/functions';
import axios from 'axios';

const getNews = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = `someTokenGoesHere`;
        const getNewsUrl = `someUrl`;
    
        const headers = await setHeaders(token, req.headers);
        const data = {example: 'data'};
        /* const { data } = await axios.get(getNewsUrl, { headers }).catch((err) => {
            err.origin = 'Error getting News';
            throw err;
        }); */

        return data;
    } catch (error) {
        next(error);
    }
};

export { getNews };
