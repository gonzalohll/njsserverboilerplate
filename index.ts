import express from 'express';
import cors from 'cors';
import router  from './src/routes/router';

//Controllers
import { getNewsRoute } from './src/controllers/getNews';

const app = express();
app.use(cors({
    origin: '*'
}));
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log('The application is listening on port ' + PORT);
})
app.use(cors());
app.use('/api/v1', router);